<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <!-- <link rel="icon" type="image/png" href="assets/img/favicon.ico"> -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>PickyMart</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- FontAwesome -->
    <link href="<?php echo base_url('assets/fontawesome/css/fontawesome.css') ?>" rel="stylesheet" />

    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="<?php echo base_url('assets/css/animate.min.css') ?>" rel="stylesheet" />

    <!--  Light Bootstrap Table core CSS    -->
    <link href="<?php echo base_url('assets/css/light-bootstrap-dashboard.css?v=1.4.0') ?>" rel="stylesheet" />


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?php echo base_url('assets/css/demo.css') ?>" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="<?php echo base_url('assets/css/pe-icon-7-stroke.css') ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/css/all.min.css') ?>" rel="stylesheet" />

</head>

<body>

    <div class="wrapper">
        <div class="sidebar">

            <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

    <div class="sidebar-wrapper" style="background-color: black;">
        <div class="logo"  style="background-color: red;">
            <a href="<?php echo base_url('index.php/Product') ?>" class="simple-text" >
                <font color="black">
                    <b>
                        PICKYMART
                    </b>
                </font>
            </a>
        </div>

        <ul class="nav">
            <li>
                <a href="<?php echo base_url('index.php/Product') ?>">
                    <div style="font-size: 1.5rem;"><i class="fas fa-store fa-2x"></i>&nbsp &nbspDashboard</div>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url('index.php/Product/add') ?>">
                    <div style="font-size: 1.5rem;">&nbsp <i class="fas fa-clipboard-list fa-2x"></i>&nbsp &nbsp &nbspTambah Barang</div>
                </a>
            </li>
            <li class="active">
                <a href="<?php echo base_url('index.php/Product/dataTransaksi') ?>">
                    <div style="font-size: 1.5rem;">&nbsp<i class="fas fa-chart-bar fa-2x"></i>&nbsp &nbsp Riwayat Transaksi</div>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url('index.php/Product/profile') ?>">
                    <div style="font-size: 1.5rem;">&nbsp<i class="fas fa-user-edit fa-2x"></i>&nbsp User Profile</div>
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="main-panel">
    <nav class="navbar navbar-default navbar-fixed">
        <div class="container-fluid" style="background-color: black;">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <a class="navbar-brand">
                    <font color="white">
                        <i class="fas fa-search"></i>
                    </font>
                </a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-left">
                    <li>
                        <input class="form-control" id="myInput" type="text" placeholder="Search.." style="margin-top: 10px;">
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right" style="background-color: red;">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                            <p> <font color="black"> <i class="fas fa-user-circle"></i>  <?php echo $this->session->userdata("username"); ?> 
                            <b class="caret"></b>
                        </font>
                    </p>

                </a>
                <ul class="dropdown-menu">
                    <li><a href="<?php echo base_url('index.php/User/logout') ?>">Log out</a></li>
                </ul>
            </li>
            <li class="separator hidden-lg"></li>
        </ul>
        </div>
    </div>
</nav>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Riwayat</h4>
                        <p class="category">menampilkan data riwayat transaksi</p>
                    </div>
                    <div class="content table-responsive table-full-width">
                        <table class="table table-hover table-striped">
                            <thead>

                                <th>
                                    <center>ID</center>
                                </th>
                                <th>
                                    <center>Nama Barang</center>
                                </th>
                                <th>
                                    <center>Harga Barang</center>
                                </th>
                                <th>
                                    <center>Jumlah Beli</center>
                                </th>
                                <th>
                                    <center>Total Harga</center>
                                </th>
                                <th>
                                    <center>Tanggal Beli</center>
                                </th>
                                <th>
                                    <center>Action</center>
                                </th>

                            </thead>
                            <tbody id="myTable">
                                <?php foreach ($transaksi as $key => $transaksi) : ?>
                                    <tr>
                                        <td>
                                            <center>
                                                <?php echo $key+1 ?>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <?php echo $transaksi->name ?>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                Rp.
                                                <?php echo $transaksi->price ?>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <?php echo $transaksi->stok ?>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                Rp. 
                                                <?php echo $transaksi->total ?>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <?php echo $transaksi->history ?>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <a onclick="deleteConfirm('<?php echo base_url('index.php/Product/deleteTransaksi/'.$transaksi->id_transaksi) ?>')"
                                                    href="#" class="btn btn-danger" data-toggle="modal"
                                                    data-target="#Delete">Delete</a>
                                                </center>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <footer class="footer">
        <div class="container-fluid">
            <p class="copyright pull-right">
                &copy;
                <script>document.write(new Date().getFullYear())</script> <a href="#">Ayzar Fachru M</a>, make a better web.
            </p>
        </div>
    </footer>

</div>
</div>

<!-- Delete Modal-->
<div class="modal fade" id="Delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Delete?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Delete" below if you are ready to delete your data.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a id="btn-delete" class="btn btn-danger" href="#">Delete</a>
            </div>
        </div>
    </div>
</div>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
      </button>
  </div>
  <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
  <div class="modal-footer">
    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
    <a class="btn btn-primary" href="<?php echo base_url('user/logout'); ?>">Logout</a>
</div>
</div>
</div>
</div>

<script>
    function deleteConfirm(url) {
        $('#btn-delete').attr('href', url);
            // $('#Delete').modal();
        }
    </script>

<script>
        $(document).ready(function(){
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>
</body>

<!--   Core JS Files   -->
<script src="<?php echo base_url('assets/js/jquery.3.2.1.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="<?php echo base_url('assets/js/chartist.min.js') ?>"></script>

<!--  Notifications Plugin    -->
<script src="<?php echo base_url('assets/js/bootstrap-notify.js') ?>"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="<?php echo base_url('assets/js/light-bootstrap-dashboard.js?v=1.4.0') ?>"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="<?php echo base_url('assets/js/demo.js') ?>"></script>

<script src="<?php echo base_url('assets/js/all.min.js') ?>" type="text/javascript"></script>

<!-- <script type="text/javascript">
    $(document).ready(function () {

        demo.initChartist();

        $.notify({
            icon: 'pe-7s-gift',
            message: "Welcome to <b>Light Bootstrap Dashboard</b>"

        }, {
                type: 'danger',
                timer: 4000
            });

    });
</script> -->

</html>