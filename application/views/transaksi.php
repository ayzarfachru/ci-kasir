<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <!-- <link rel="icon" type="image/png" href="../assets/img/favicon.ico"> -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>PickyMart</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="<?php echo base_url('assets/css/animate.min.css') ?>" rel="stylesheet" />

    <!--  Light Bootstrap Table core CSS    -->
    <link href="<?php echo base_url('assets/css/light-bootstrap-dashboard.css?v=1.4.0') ?>" rel="stylesheet" />


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?php echo base_url('assets/css/demo.css') ?>" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url('assets/css/pe-icon-7-stroke.css') ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/css/all.min.css') ?>" rel="stylesheet" />
    <script>
        var x = 0;
        var y = 0;
        var z = 0;
        function calc(obj) {
            var e = obj.id.toString();
            if (e == 'price') {
                x = Number(obj.value);
                y = Number(document.getElementById('stok1').value);
            } else {
                x = Number(document.getElementById('price').value);
                y = Number(obj.value);
            }
            z = x * y;
            document.getElementById('total').value = z;
            // document.getElementById('update').innerHTML = z;
        }
    </script>
</head>

<body>

    <div class="wrapper">
        <div class="sidebar">

            <!--   you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple" -->


            <div class="sidebar-wrapper" style="background-color: black;">
                <div class="logo" style="background-color: red;">
                    <a href="<?php echo base_url('index.php/Product') ?>" class="simple-text" >
                        <font color="black">
                            <b>
                                PICKYMART
                            </b>
                        </font>
                    </a>
                </div>

                <ul class="nav">
                    <li class="active">
                        <a href="<?php echo base_url('index.php/Product') ?>">
                            <div style="font-size: 1.5rem;"><i class="fas fa-store fa-2x"></i>&nbsp &nbspDashboard</div>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('index.php/Product/add') ?>">
                            <div style="font-size: 1.5rem;">&nbsp <i class="fas fa-clipboard-list fa-2x"></i>&nbsp &nbsp &nbspTambah Barang</div>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('index.php/Product/dataTransaksi') ?>">
                            <div style="font-size: 1.5rem;">&nbsp<i class="fas fa-chart-bar fa-2x"></i>&nbsp &nbsp Riwayat Transaksi</div>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('index.php/Product/profile') ?>">
                            <div style="font-size: 1.5rem;">&nbsp<i class="fas fa-user-edit fa-2x"></i>&nbsp User Profile</div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="main-panel">
            <nav class="navbar navbar-default navbar-fixed">
                <div class="container-fluid" style="background-color: black;">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse">

                        <ul class="nav navbar-nav navbar-right" style="background-color: red;">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                                    <p> <font color="black"> <i class="fas fa-user-circle"></i>  <?php echo $this->session->userdata("username"); ?> 
                                    <b class="caret"></b>
                                </font>
                            </p>

                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url('index.php/User/logout') ?>">Log out</a></li>
                        </ul>
                    </li>
                    <li class="separator hidden-lg"></li>
                </ul>
            </div>
        </div>
    </nav>


    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="header">
                            <div>
                                <a href="<?php echo base_url('index.php/Product') ?>" style="float: right" class="text-primary"><i
                                    class="fas fa-arrow-left"></i><b>Back</b></a>
                                    <h4 class="title">Transaksi</h4>
                                </div>
                                <p class="category">membeli barang</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <div class="container-fluid">
                                    <form action="#" method="post" enctype="multipart/form-data" id="transaksi"><br>
                                        <input type="hidden" name="id" value="<?php echo $product->id_product ?>" />
                                        <div class="form-group">
                                            <label for="name"><b>Nama Barang : </b></label>
                                            <input type="text" class="form-control <?php echo form_error('name') ? 'is-invalid':'' ?>"
                                            id="name" placeholder="Enter name" name="name" value="<?php echo $product->name ?>" readonly />
                                            <div class="invalid-feedback">
                                                <?php echo form_error('name') ?>
                                            </div>
                                        </div><br>
                                        <div class="form-group">
                                            <label for="price"><b>Harga Barang : </b></label>
                                            <input type="number" class="form-control <?php echo form_error('price') ? 'is-invalid':'' ?>"
                                            id="price" placeholder="Enter price" name="price" value="<?php echo $product->price ?>" onkeyup="calc(this)" readonly />
                                            <div class="invalid-feedback">
                                                <?php echo form_error('price') ?>
                                            </div>
                                        </div><br>
                                        <div class="form-group">
                                            <label for="stok"><b>Jumlah Barang : </b></label>
                                            <input type="number" class="form-control <?php echo form_error('stok') ? 'is-invalid':'' ?>"
                                            id="stok" placeholder="Enter stock" name="stok" value="<?php echo $product->stok ?>" readonly/>
                                            <div class="invalid-feedback">
                                                <?php echo form_error('stok') ?>
                                            </div>
                                        </div><br>
                                        <div class="form-group">
                                            <label for="stok"><b>Jumlah Beli : </b></label>
                                            <input type="number" class="form-control <?php echo form_error('stok1') ? 'is-invalid':'' ?>"
                                            id="stok1" placeholder="Enter stock" name="stok1" value="" onkeyup="calc(this)" required />
                                            <div class="invalid-feedback">
                                                <?php echo form_error('stok1') ?>
                                            </div>
                                        </div><br>
                                        <div class="form-group">
                                            <label for="stok"><b>Total Harga : </b></label>
                                            <input type="number" class="form-control <?php echo form_error('stok1') ? 'is-invalid':'' ?>"
                                            id="total" placeholder="Total value" name="total" value="0" readonly/>
                                            <div class="invalid-feedback">
                                                <?php echo form_error('stok1') ?>
                                            </div>
                                        </div><br>
                                        <input type="submit" name="btn" class="btn btn-primary" style="float: right"
                                        value="Buy" />
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="container-fluid">
            <p class="copyright pull-right">
                &copy;
                <script>document.write(new Date().getFullYear())</script> <a href="#">Ayzar Fachru M</a>, make a better web.
            </p>
        </div>
    </footer>


</div>
</div>

<!-- modal delete -->
<div class="modal fade" id="Delete" tabindex="-1" role="dialog" aria-labelledby="deleteModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="deleteModal">Hapus barang?</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                </div>
                <div class="modal-body">
                    <button type="button" class="btn btn-default" style="float: right; margin: 7px" data-dismiss="modal">Tidak</button>
                    <form action="delete.php" method="post">
                        <input type="hidden" name="id_barang" id="deleteAction" value="0">
                        <button type="submit" class="btn btn-danger" style="float: right; margin: 7px">Ya</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
          </button>
      </div>
      <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <a class="btn btn-primary" href="<?php echo base_url('user/logout'); ?>">Logout</a>
    </div>
</div>
</div>
</div>

<!-- <script>
function calculateAmount(val)
{ 
    var price = document.getElementById('price');
    var total = val * price ;
//display the result
var divobj = document.getElementById('total');
divobj.value = total;
}
</script> -->

</body>

<!--   Core JS Files   -->
<script src="<?php echo base_url('assets/js/jquery.3.2.1.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="<?php echo base_url('assets/js/chartist.min.js') ?>"></script>

<!--  Notifications Plugin    -->
<script src="<?php echo base_url('assets/js/bootstrap-notify.js') ?>"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="<?php echo base_url('assets/js/light-bootstrap-dashboard.js?v=1.4.0') ?>"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="<?php echo base_url('assets/js/demo.js') ?>"></script>

<script src="<?php echo base_url('assets/js/all.min.js') ?>" type="text/javascript"></script>


</html>