<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <!-- <link rel="icon" type="image/png" href="assets/img/favicon.ico"> -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Toserba</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="<?php echo base_url('assets/css/animate.min.css') ?>" rel="stylesheet" />

    <!--  Light Bootstrap Table core CSS    -->
    <link href="<?php echo base_url('assets/css/light-bootstrap-dashboard.css?v=1.4.0') ?>" rel="stylesheet" />


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?php echo base_url('assets/css/demo.css') ?>" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url('assets/css/pe-icon-7-stroke.css') ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/css/all.min.css') ?>" rel="stylesheet" />

</head>

<body>

    <div class="wrapper">
        <div class="sidebar" data-color="red" data-image="<?php echo base_url('assets/img/sidebar-4.jpg') ?>">

            <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

            <div class="sidebar-wrapper">
                <div class="logo">
                    <a href="<?php echo base_url('index.php/Product') ?>" class="simple-text">
                        TOSERBA
                    </a>
                </div>

                <ul class="nav">
                    <li class="active">
                        <a href="<?php echo base_url('index.php/Product') ?>">
                            <i class="pe-7s-note2"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('index.php/Product/add') ?>">
                            <i class="pe-7s-note"></i>
                            <p>Tambah Barang</p>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('index.php/Product/dataTransaksi') ?>">
                            <i class="pe-7s-cart"></i>
                            <p>Riwayat Transaksi</p>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('profile') ?>">
                            <i class="pe-7s-user"></i>
                            <p>User Profile</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="main-panel">
            <nav class="navbar navbar-default navbar-fixed">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">Dashboard</a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <form action="<?php echo base_url('product/searchProduct') ?>" method="post">
                                    <input type="text" class="form-control" placeholder="search" name="keyword" style="margin-top: 10px" />
                                </form>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                                    <p><i class="far fa-user"></i>  <?php echo $this->session->userdata("username"); ?>
                                         <b class="caret"></b>
                                    </p>

                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo base_url('profile') ?>">User Profile</a></li>
                                    <li><a href="#">Setting</a></li>
                                    <li class="divider"></li>
                                    <li><a href="index.php/User/logout">Log out</a></li>
                                </ul>
                            </li>
                            <li class="separator hidden-lg"></li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="header">
                                    <h4 class="title">Data Barang</h4>
                                    <p class="category">menampilkan data barang</p>
                                </div>
                                <div class="content table-responsive table-full-width">
                                    <table class="table table-hover table-striped">
                                        <thead>

                                            <th>
                                                <center>ID</center>
                                            </th>
                                            <th>
                                                <center>Nama Barang</center>
                                            </th>
                                            <th>
                                                <center>Harga Barang</center>
                                            </th>
                                            <th>
                                                <center>Jumlah Barang</center>
                                            </th>
                                            <th>
                                                <center>Action</center>
                                            </th>

                                        </thead>
                                        <tbody>
                                            <?php foreach ($products as $key => $product) : ?>
                                            <tr>
                                                <td>
                                                    <center>
                                                        <?php echo $key+1 ?>
                                                    </center>
                                                </td>
                                                <td>
                                                    <center>
                                                        <?php echo $product->name ?>
                                                    </center>
                                                </td>
                                                <td>
                                                    <center>
                                                        Rp.
                                                        <?php echo $product->price ?>
                                                    </center>
                                                </td>
                                                <td>
                                                    <center>
                                                        <?php echo $product->stok ?>
                                                    </center>
                                                </td>
                                                <td>
                                                    <center>
                                                        <a href="<?php echo base_url('product/edit/'.$product->id_product) ?>"><button
                                                                class="btn btn-primary">Edit</button></a>
                                                        <a onclick="deleteConfirm('<?php echo base_url('index.php/Product/delete/'.$product->id_product) ?>')"
                                                            href="#" class="btn btn-danger" data-toggle="modal"
                                                            data-target="#Delete">Delete</a>
                                                        <a href="<?php echo base_url('index.php/Product/transaksi/'.$product->id_product) ?>"><button
                                                            class="btn btn-warning">Transaksi</button></a>
                                                    </center>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <footer class="footer">
                <div class="container-fluid">
                    <p class="copyright pull-right">
                        &copy;
                        <script>document.write(new Date().getFullYear())</script> <a href="#">Ayzar Fachru M</a>, make a better web.
                    </p>
                </div>
            </footer>

        </div>
    </div>

    <!-- Delete Modal-->
    <div class="modal fade" id="Delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Delete?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Delete" below if you are ready to delete your data.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a id="btn-delete" class="btn btn-danger" href="#">Delete</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <a class="btn btn-primary" href="<?php echo base_url('index.php/User/logout'); ?>">Logout</a>
      </div>
    </div>
  </div>
</div>

    <script>
        function deleteConfirm(url) {
            $('#btn-delete').attr('href', url);
            // $('#Delete').modal();
        }
    </script>

</body>

<!--   Core JS Files   -->
<script src="<?php echo base_url('assets/js/jquery.3.2.1.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="<?php echo base_url('assets/js/chartist.min.js') ?>"></script>

<!--  Notifications Plugin    -->
<script src="<?php echo base_url('assets/js/bootstrap-notify.js') ?>"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="<?php echo base_url('assets/js/light-bootstrap-dashboard.js?v=1.4.0') ?>"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="<?php echo base_url('assets/js/demo.js') ?>"></script>

<script src="<?php echo base_url('assets/js/all.min.js') ?>" type="text/javascript"></script>

<!-- <script type="text/javascript">
    $(document).ready(function () {

        demo.initChartist();

        $.notify({
            icon: 'pe-7s-gift',
            message: "Welcome to <b>Light Bootstrap Dashboard</b>"

        }, {
                type: 'danger',
                timer: 4000
            });

    });
</script> -->

</html>