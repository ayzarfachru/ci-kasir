<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('muser');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

    public function index() {
        $this->load->view('login');
    }

    public function action_login() {
        
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $where = array(
            'username' => $username,
            'password' => $password,
        );

        $check = $this->muser->cek_login("user", $where)->num_rows();

        if ($check > 0) {
            $data_session = array (
                'username' => $username,
                // 'email' => $email,
                'status' => 'login'
            );

            $this->session->set_userdata($data_session);
            redirect(base_url('index.php/Product'));
        } else {
            echo "username atau password salah";
        }

    }

    public function game_snake() {
        $this->load->view('game');
    }


    public function register() {
        $this->load->view('register');
    }

    public function action_register() {
        $register = $this->muser;
        $validation = $this->form_validation;
        $validation->set_rules($register->rules());

        if ($validation->run()) {
            $register->register();
            $this->session->set_flashdata('success', 'register berhasil');
            redirect(base_url('index.php/User'));
        }

        $this->load->view('register');
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect(base_url('index.php/User'));
    }

}

/* End of file User.php */
